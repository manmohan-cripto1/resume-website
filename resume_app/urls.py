# resume_app/urls.py
from django.urls import path
from . import views

urlpatterns = [
    path('education/', views.education_view, name='education'),
    path('experience/', views.experience_view, name='experience'),
]
