# resume_app/views.py
from django.shortcuts import render
from .models import Education, Experience

def education_view(request):
    educations = Education.objects.all()
    return render(request, 'education.html', {'educations': educations})

def experience_view(request):
    experiences = Experience.objects.all()
    return render(request, 'experience.html', {'experiences': experiences})

