from django.db import models

class Resume(models.Model):
    full_name = models.CharField(max_length=100)
    job_title = models.CharField(max_length=100)
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    linkedin_url = models.URLField()
    summary = models.TextField()
    experiences = models.TextField()
    educations = models.TextField()
    # You can add more fields as needed
