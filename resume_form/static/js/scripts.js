// scripts.js

const darkModeToggle = document.getElementById('dark-mode-toggle');
const resumeForm = document.getElementById('resume-form');
const body = document.body;

darkModeToggle.addEventListener('change', () => {
  body.classList.toggle('dark-mode');
});

// Optional: If you want to save the user's preference in localStorage
if (localStorage.getItem('dark-mode')) {
  body.classList.add('dark-mode');
}
