from django.urls import path
from . import views

urlpatterns = [
    path('form/', views.resume_form_view, name='resume_form'),
    path('form/success/', views.resume_success_view, name='resume_success'),
    # Add other URL patterns for your app here if needed
]
