from django.shortcuts import render, redirect
from .forms import ResumeForm

def resume_form_view(request):
    if request.method == 'POST':
        form = ResumeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('resume_success')  # Redirect to a success page after form submission
    else:
        form = ResumeForm()

    return render(request, 'resume_form.html', {'form': form})

def resume_success_view(request):
    return render(request, 'resume_success.html')
